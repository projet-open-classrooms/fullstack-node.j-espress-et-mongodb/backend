# backend

Projet Back-End Node.js et Express connecté à une base de donnée MongoDB

**Setup de l'environnement pour exploiter le projet.**

**Installation de Node**

Accédez à [NodeJS.org](https://nodejs.org/en/) pour télécharger puis installer la dernière version de Node. 
Cela a pour effet d'installer le runtime JavaScript de Node, ce qui vous permet par là même d'exécuter les serveurs Node. 
Cela installe également Node Package Manager ou npm, outil précieux pour l'installation des packages nécessaires à la création de vos projets.

**Demarrer le serveur Node**

Démarrez le serveur en exécutant `node server` à partir de la ligne de commande.

Cette façon de faire ne vous permettra pas de redemarrer automatiquement le serveur Node après avoir apporté des modification à votre code.
Pour ce fait il vous faut installer ***nodemon***, qui lui sera capable de redemarrer notre serveur après une quelconque modification du code.

**Installation de nodemon**

Pour ce faire, exécutez la commande suivante :

`npm install -g nodemon`

Désormais, au lieu d'utiliser `node server` pour démarrer votre serveur, vous pouvez utiliser `nodemon server`.
