const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const Product = require('./models/Product');

const app2 = express();

mongoose.connect('mongodb+srv://ktacent:admin@test-8zoul.mongodb.net/test?retryWrites=true&w=majority',
  { useNewUrlParser: true,
    useUnifiedTopology: true })
  .then(() => console.log('Connexion à MongoDB réussie !'))
  .catch(() => console.log('Connexion à MongoDB échouée !'));

app2.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
  next();
});

app2.use(bodyParser.json());

app2.post('/api/products', (req, res, next) => {
    delete req.body._id;
    const product = new Product({
        ...req.body
    });
    product.save()
    .then(product => res.status(201).json({ product }))
    .catch(error => res.status(400).json({error}))
});

app2.get('/api/products', (req, res, next) => {
    Product.find()
    .then(products => res.status(200).json({ products: products}))
    .catch(error => res.status(400).json({error}));
});

app2.put('/api/products/:id', (req, res, next) =>{
    Product.updateOne({ _id: req.params.id}, { ...req.body, _id: req.params.id})
    .then(() => res.status(200).json({message: 'Modified !'}))
    .catch(error => res.status(400).json({error}))
});

app2.delete('/api/products/:id', (req, res, next) => {
  Product.deleteOne({ _id: req.params.id })
    .then(() => res.status(200).json({ message: 'Deleted !'}))
    .catch(error => res.status(400).json({ error }));
});

app2.get('/api/products/:id', (req, res, next) => {
  Product.findOne({ _id: req.params.id })
    .then(product => res.status(200).json({ product: product}))
    .catch(error => res.status(404).json({ error }));
});



module.exports = app2;